package gigcity

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/russross/blackfriday"
)

// Event contains details about GDG events, used when preforming read/write ops
// to the datastore
type Event struct {
	Name     string
	Datetime string
	Location EventLoc
	Links    EventLinks
	Details  template.HTML
}

type EventLoc struct {
	Where    string `json:"addr"`
	Details  string `json:"loc_details"`
	EmbedURL string `json:"map_embed"`
	MapURL   string `json:"map_url"`
}

type EventLinks struct {
	GooglePlus string `json:"google_plus"`
	Meetup     string `json:"meetup"`
	Video      string `json:"video"`
	Slides     string `json:"slides"`
}

type EventJSON struct {
	EventName     string     `json:"event_name"`
	Datetime      time.Time  `json:"datetime"`
	EventLocation EventLoc   `json:"event_location"`
	EventLinks    EventLinks `json:"event_links"`
	EventDetails  string     `json:"event_details"`
}

type EventList struct {
	URI      string
	Name     string
	Datetime string
}

// ByDate implements sort.Interface for []EventList based on the Datetime field.
type ByDate []EventList

func (a ByDate) Len() int      { return len(a) }
func (a ByDate) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByDate) Less(i, j int) bool {
	t1, _ := time.Parse("2006-01-02 3:04 pm", a[i].Datetime)
	t2, _ := time.Parse("2006-01-02 3:04 pm", a[j].Datetime)
	return t1.Before(t2)
}

// Handles requests to /events
func eventListHandler(w http.ResponseWriter, r *http.Request) {
	files, err := ioutil.ReadDir("events")
	if err != nil {
		errorHandler(w, r, http.StatusInternalServerError, "could not read events directory")
		return
	}

	var list []EventList
	for _, file := range files {
		logHandler("INFO", fmt.Sprintf("file %v found", file))
		path := fmt.Sprintf("events/%s", file.Name())
		b, err := ioutil.ReadFile(path)
		if err != nil {
			logHandler("ERROR", fmt.Sprintf("failed to read %s: %s", path, err.Error()))
			continue
		}
		var e EventJSON
		err = json.Unmarshal(b, &e)
		if err != nil {
			logHandler("ERROR", fmt.Sprintf("failed to parse JSON from %s: %s", path, err.Error()))
			continue
		}
		URLParts := strings.Split(file.Name(), "_")

		var item EventList
		item.URI = URLParts[1]
		item.Name = e.EventName
		item.Datetime = e.Datetime.Format("2006-01-02 3:04 pm")
		list = append(list, item)
	}
	sort.Sort(sort.Reverse(ByDate(list)))

	page := template.Must(template.ParseFiles(
		"static/_base.html",
		"static/events.html",
	))

	if err := page.Execute(w, list); err != nil {
		errorHandler(w, r, http.StatusInternalServerError, err.Error())
		return
	}
}

func eventHandler(w http.ResponseWriter, r *http.Request) {
	URI := r.URL.Path[len("/events/"):]
	if URI == "" {
		http.Redirect(w, r, "/events", http.StatusSeeOther)
		return
	}

	fh, err := getEventDetails(URI)
	if err != nil {
		errorHandler(w, r, http.StatusInternalServerError, err.Error())
		return
	} else if fh == nil && err == nil {
		errorHandler(w, r, http.StatusNotFound, "")
		return
	}

	var e EventJSON
	b, err := ioutil.ReadFile(fmt.Sprintf("events/%s", fh.Name()))
	if err != nil {
		errorHandler(w, r, http.StatusInternalServerError, err.Error())
		return
	}
	err = json.Unmarshal(b, &e)
	if err != nil {
		errorHandler(w, r, http.StatusInternalServerError, err.Error())
		return
	}

	var event Event
	event.Name = e.EventName
	event.Datetime = e.Datetime.Format("2006-01-02 3:04 pm")
	event.Location = e.EventLocation
	event.Links = e.EventLinks
	details := blackfriday.MarkdownCommon([]byte(e.EventDetails))
	event.Details = template.HTML(string(details))

	page := template.Must(template.ParseFiles(
		"static/_base.html",
		"static/view-event.html",
	))

	if err := page.Execute(w, event); err != nil {
		errorHandler(w, r, http.StatusInternalServerError, err.Error())
		return
	}
}

func getEventDetails(URI string) (os.FileInfo, error) {
	dir, err := ioutil.ReadDir("events")
	if err != nil {
		return nil, fmt.Errorf("couldn't read events directory: %v", err)
	}

	for _, file := range dir {
		parts := strings.Split(file.Name(), "_")
		if parts[1] == URI {
			return file, nil
		}
	}

	return nil, nil
}

/*
// geteventhandler handles requests for /events/:event
func getEventHandler(w http.ResponseWriter, r *http.Request) {
	type Content struct {
		EventDetails Event
		LocDetails   Location
	}

	var context Content
	c := appengine.NewContext(r)
	eventID := r.URL.Query().Get(":event")
	if eventID == "" {
		errorHandler(w, r, http.StatusInternalServerError, "no event ID found in URL")
		return
	}
	q := datastore.NewQuery("Events").Filter("ID =", eventID)
	t := q.Run(c)
	for {
		var e Event
		_, err := t.Next(&e)
		if err == datastore.Done {
			break
		}
		if err != nil {
			c.Errorf("fetching event details failed: %v", err)
			break
		}

		// format the datestamp
		t, err := time.Parse("2006-01-02T15:04", e.Datetime)
		if err != nil {
			errorHandler(w, r, http.StatusInternalServerError, err.Error())
			return
		}

		e.Datetime = t.Format("2006-01-02 3:04 PM")
		context.EventDetails = e
	}

	q = datastore.NewQuery("Locations").Filter("ID =", context.EventDetails.LocID)
	t = q.Run(c)
	for {
		var l Location
		_, err := t.Next(&l)
		if err == datastore.Done {
			break
		}
		if err != nil {
			c.Errorf("fetching location details failed: %v", err)
			break
		}

		context.LocDetails = l
	}

	page := template.Must(template.ParseFiles(
		"static/_base.html",
		"static/view-event.html",
	))

	if err := page.Execute(w, context); err != nil {
		errorHandler(w, r, http.StatusInternalServerError, err.Error())
		return
	}
}
*/
